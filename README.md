﻿	>>>>>>>>>>>>>>>>BÀI TẬP LỚN KĨ THUẬT LẬP TRÌNH NHÓM 28 CHỦ ĐỀ 1 <<<<<<<<<<<
	
	/*
		Các thành viên bao gồm
			1,Đồng Quang linh   : 20162385
			2,Đỗ Minh hiếu 	    : 20161505
			3,Nguyễn Tiến quang : 20163312
	*/
	Project bao gồm các file 
		- file btl_ktlt.cpp chứa nội dung code của cả chương trình (trong code có tiếng việt nên khi mở file để định dạng UTF8)
		-các file chuoi1.txt và chuoi2.txt là 2 file đầu vào chứa chuỗi 1 và chuỗi 2
		-1 file là sinhTest.cpp là 1 file phụ.giúp tạo ra dũ liệu để test
		-1 flie là timeTest.txt là file chưa kết quả test thuật toán trên các bộ dữ liệu tăng dần ngẫu nhiên
	HƯỚNG DẪN TEST 
		- mở file "btl_ktlt.cpp" để test với dữ liệu đầu vào đưa vào 2 file là "chuoi1.txt" 
		và "chuoi2.txt"
		
		-Ngoài ra chúng tôi còn hỗ trợ người dùng bằng 1 chương trình phụ là "sinhTest.cpp" trong chương trình đó sẽ ghi
    ra 2 file "chuoi1.txt" và "chuoi2.txt" với số lượng kí tự do người test muốn có : chỉ cần thay 
    đổi tham số ở dòng code thứ 13 và 14 ứng với số lượng kí tự mong muốn và chương trình sẽ tạo ra
    ngẫu nhiên số kí tự đó.Nếu bạn muốn 2 file giống nhau,thì đơn giản chỉ cần tạo ra 1 file và copy paste sang file2!!
		
		